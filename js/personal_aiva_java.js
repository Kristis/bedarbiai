
 $(document).ready(function(){

	$('.carousel').carousel();
    $('.carousel.full-carousel').carousel({fullWidth: true});
	 
new Chartist.Pie('.ct-series-a', {
labels: ['BANANAI 20%', 'OBUOLIAI 10%', 'VYNUOGĖS 30%', 'MANGO 40%'],
  series: [20, 10, 30, 40]
  
}, {
  donut: true,
  donutWidth: 60,
  donutSolid: true,
  startAngle: 270,
  total: 200,
  showLabel: true
});


new Chartist.Pie('.ct-series-b', {
  series: [40, 10, 30, 40],
  labels: ['LATTE 40', 'WITH MILK 10', 'CAPUCHINO 30', 'AMERICANO 40'],
  
}, {
  donut: true,
  donutWidth: 60,
  donutSolid: true,
  startAngle: 270,
  total: 200,
  showLabel: true
});

new Chartist.Pie('.ct-series-c', {
labels: ['MAISTUI 40%', 'RŪBAMS 10%', 'BŪSTUI 30%', 'KELIONĖMS 20%'],
  series: [40, 10, 30, 20],
}, {
  donut: true,
  donutWidth: 60,
  donutSolid: true,
  startAngle: 270,
  total: 200,
  showLabel: true
});
  
  
var data1 = {
  
  series: [20, 15, 40],
  labels: ['20 GUMINUKŲ', '15 OBUOLIŲ', '40 SALDAINIŲ'],
};

var options1 = {
  labelInterpolationFnc: function(value) {
    return value[0]
  }
};

var responsiveOptions = [
  ['screen and (min-width: 640px)', {
    chartPadding: 30,
    labelOffset: 100,
    labelDirection: 'explode',
    labelInterpolationFnc: function(value) {
      return value;
    }
  }],
  ['screen and (min-width: 1024px)', {
    labelOffset: 80,
    chartPadding: 20
  }]
];

new Chartist.Pie('.ct-chart1', data1, options1, responsiveOptions);

 });
 
 
 var data = {
  labels: ['SU GRYBAIS 20', 'SU VARŠKE 15', 'SU ŠOKOLADU 40'],
  series: [20, 15, 40]
};

var options = {
  labelInterpolationFnc: function(value) {
    return value[0]
  }
};

var responsiveOptions = [
  ['screen and (min-width: 640px)', {
    chartPadding: 30,
    labelOffset: 100,
    labelDirection: 'explode',
    labelInterpolationFnc: function(value) {
      return value;
    }
  }],
  ['screen and (min-width: 1024px)', {
    labelOffset: 80,
    chartPadding: 20
  }]
];

new Chartist.Pie('.ct-chart', data, options, responsiveOptions);

 $('.carousel.carousel-slider').carousel({
    fullWidth: false,
    indicators: true
  });
  
